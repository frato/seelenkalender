# seelenkalender
This little program should show the verse of the actual week based on the calendar of the soul of Rudolf Steiner.
The calendar of the soul is a set of 52 verses. It starts on easter sunday and changes every sunday.
There are English translations on http://calendarofthesoul.net/ or http://wn.rsarchive.org/Articles/GA040/English/AP1982/GA040_index.html or http://antroposofi.org/TomMellett/.
I have nothing to do with these pages. I found them by accident through startpage search.
